﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using MvvmCross.Droid.Support.V7.AppCompat;
using MvvmCross.ViewModels;
using TestApp.Core;

namespace TestApp.Droid
{
    public class Setup : MvxAppCompatSetup
    {
        protected override IMvxApplication CreateApp() => new App();

        protected override void InitializeLastChance()
        { }
    }

   
}