﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acr.UserDialogs;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Droid.Support.V7.AppCompat;

namespace TestApp.Droid.View
{
    [Activity(
        Theme = "@style/AppTheme",
       MainLauncher = true,
       NoHistory = true,
       ScreenOrientation = ScreenOrientation.Portrait)]
    public class SplashScreenActivity : MvxSplashScreenAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            UserDialogs.Init(this);
            base.OnCreate(bundle);
        }
    }
}