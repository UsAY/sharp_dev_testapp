﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acr.UserDialogs;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Droid.Support.V7.AppCompat;
using MvvmCross.Platforms.Android.Views;
using TestApp.Core.ViewModel;

namespace TestApp.Droid.View
{
    [Activity(Theme = "@style/AppTheme", Label = "RegisterActivity",NoHistory =true)]
    public class RegisterActivity : MvxAppCompatActivity<RegisterViewModel>
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.registrationlayout);
          
        }
    }
}