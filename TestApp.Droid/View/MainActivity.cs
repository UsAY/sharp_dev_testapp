﻿using Android.App;
using Android.Widget;
using Android.OS;
using MvvmCross.Droid.Support.V7.AppCompat;
using TestApp.Core.ViewModel;
using MvvmCross.Platforms.Android.Views;
using MvvmCross;

namespace TestApp.Droid
{
    [Activity(Theme = "@style/AppTheme",Label ="")]
    public class MainActivity : MvxActivity<MainViewModel>
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            
            SetContentView(Resource.Layout.Main);
        }
    }
}

