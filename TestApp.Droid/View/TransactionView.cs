﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Droid.Support.V7.AppCompat;
using TestApp.Core.ViewModel;

namespace TestApp.Droid.View
{
    [Activity(Theme = "@style/AppTheme",Label = "TransactionView")]
    public class TransactionView:MvxAppCompatActivity<TransactionViewModel>
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.TransactionLayout);

        }
    }
}