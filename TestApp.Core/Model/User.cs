﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestApp.Core.Model
{
   public class User
    {

        public string Id { get; set; }
        public string Name { get; set; }
        public override string ToString()
        {
            return Name;
        }
    }
}
