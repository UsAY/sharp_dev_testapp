﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestApp.Core.Model
{
   public class TransactionData
    {
        public string Name { get; set; }
        public double Amount { get; set; }
    }
}
