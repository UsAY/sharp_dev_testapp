﻿using Newtonsoft.Json;
using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestApp.Core.Model
{
   public class TransTokenItem
    {
        public string Id { get; set; }
        public string Date { get; set; }
        [DeserializeAs(Name = "username")]
        [JsonProperty("username")]
        public string UserName { get; set; }
        public double Amount { get; set; }
        public double Balance { get; set; }
    }
}
