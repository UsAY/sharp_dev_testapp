﻿using Newtonsoft.Json;
using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestApp.Core.Model
{
    public class TransList
    {
        [DeserializeAs(Name = "trans_token")]
        [JsonProperty("trans_token")]
       public List<TransTokenItem> TransTokens { get; set; }
    }
}
