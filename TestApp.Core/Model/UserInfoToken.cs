﻿using Newtonsoft.Json;
using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestApp.Core.Model
{
  public  class UserInfoToken
    {
        [DeserializeAs(Name = "user_info_token")]
        [JsonProperty("user_info_token")]
        public UserInfo UserInfo { get; set; }
    }
}
