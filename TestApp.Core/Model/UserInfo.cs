﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestApp.Core.Model
{
    public class UserInfo: User
    {
        public string Email { get; set; }
        public string Balance { get; set; }
    }
}
