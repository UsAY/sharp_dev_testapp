﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestApp.Core.Model
{
    public class UserToken
    {
        public UserToken() { }

        [JsonProperty("id_token")]
        public string IdToken { get; set; }
    }
}
