﻿
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestApp.Core.Model;
using TestApp.Core.Services.Interface;

namespace TestApp.Core.Services
{
    public class PWService : IPWService
    {
        private readonly ISession _session;


        public PWService(ISession session)
        {
            _session = session;
        }



        public async Task<TransToken> CreateTransaction(string name, double amount)
        {
            var request = new RestRequest("api/protected/transactions", Method.POST);
            request.AddParameter("name", name);
            request.AddParameter("amount", amount);
            return await _session.Execute<TransToken>(request,true);
        }

        public async Task<TransList> TransactionsList()
        {
            var request = new RestRequest("api/protected/transactions", Method.GET);
          
            return await  _session.Execute<TransList>(request, true);
        }
    }
}
