﻿
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TestApp.Core.Exceptions;
using TestApp.Core.Model;
using TestApp.Core.Services.Interface;

namespace TestApp.Core.Services
{
    public class Session : ISession
    {
        private readonly IRestClient _client;
        public string IdToken { get; private set; }
        public IRestClient Client => _client;

        private const string BaseUrl = "http://193.124.114.46:3001/";

        

        public Session()
        {
            _client = new RestClient(BaseUrl);
        }

        public async Task<T> Execute<T>(IRestRequest request, bool isAuth) where T : new()
        {
         if(isAuth) request.AddHeader("Authorization", string.Format("Bearer {0}", IdToken));
            return await GetResponce<T>(request);
        }

        private async Task<T> GetResponce<T>(IRestRequest request) where T : new()
        {
            var response = await _client.ExecuteTaskAsync<T>(request);
            if (response != null && (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Created))
            {
                return response.Data;
            }
            else if (response != null && (response.StatusCode == HttpStatusCode.BadRequest || response.StatusCode == HttpStatusCode.Unauthorized))
            {
               
                throw new RegistrationException(response.Content);
            }
            throw new Exception("");
        }

        public async Task<UserToken> CreateSession(string username, string password, string email)
        {
            var request = new RestRequest("users", Method.POST);
            request.AddParameter("username", username);
            request.AddParameter("password", password);
            request.AddParameter("email", email);
            return await CreateSession(request);
        }

        public async Task<UserToken> CreateSession(IRestRequest request)
        {
            var result= await Execute<UserToken>(request,false);
            IdToken = result.IdToken;
            return result;
        }
    }
}
