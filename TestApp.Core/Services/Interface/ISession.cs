﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestApp.Core.Model;

namespace TestApp.Core.Services.Interface
{
   public interface ISession
    {
        Task<UserToken> CreateSession(string username, string password, string email);
        Task<UserToken> CreateSession(IRestRequest request);
        Task<T> Execute<T>(IRestRequest request, bool isAuth) where T : new();

        IRestClient Client { get; }
        string IdToken { get; }
    }
}
