﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestApp.Core.Model;

namespace TestApp.Core.Services.Interface
{
    public interface IPwUserService
    {
       
        Task<UserToken> RegisterUser(string username, string password, string email);
        Task<bool> RegisterUserB(string username, string password, string email);
        Task<UserToken> Loggin(string password, string email );
        Task<bool> LogginB(string password, string email);
        Task<UserInfoToken> LoggedUserInfo();
        Task<IEnumerable<User>> GetUsersList(string filterTerm);
        Task UpdateData();
        UserInfo UserInfo { get; set; }
        TransList TransList { get; set; }
        event EventHandler ChangeInfoEvent;


    }
}
