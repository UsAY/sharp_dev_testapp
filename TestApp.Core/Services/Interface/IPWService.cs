﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestApp.Core.Model;

namespace TestApp.Core.Services.Interface
{
   public interface IPWService
    {
        Task<TransToken> CreateTransaction(string name, double amount);
        Task<TransList> TransactionsList();
    }
}
