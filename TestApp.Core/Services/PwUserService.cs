﻿
using MvvmCross.ViewModels;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TestApp.Core.Model;
using TestApp.Core.Services.Interface;

namespace TestApp.Core.Services
{
  public  class PwUserService:MvxNotifyPropertyChanged, IPwUserService
    {

        private readonly ISession _session;
        private readonly IPWService _pWService;
        public event EventHandler ChangeInfoEvent;


        public PwUserService(ISession session, IPWService pWService)
        {
            _session = session;
            _pWService = pWService;
            var tsc = UpdateData();
          
        }

        private UserInfo _userInfo;
        public UserInfo UserInfo
        {
            get { return _userInfo; }
            set
            {
                _userInfo = value;
                RaisePropertyChanged(()=>UserInfo);
                ChangeInfoEvent?.Invoke(this, EventArgs.Empty);

            }
        }

        private TransList _transList;
        public TransList TransList
        {
            get => _transList;
            set {
                _transList = value;
                RaisePropertyChanged(() => UserInfo);
                ChangeInfoEvent?.Invoke(this, EventArgs.Empty);

            } }

        public async Task UpdateData()
        {
            UserInfo= (await LoggedUserInfo()).UserInfo;
            TransList =await _pWService.TransactionsList();
            Debug.WriteLine("Balance " + UserInfo.Balance);
        }

        public async Task<UserInfoToken> LoggedUserInfo()
        {
            var request = new RestRequest("api/protected/user-info", Method.GET);
            
         return await _session.Execute<UserInfoToken>(request,true);
           
        }

        public async Task<UserToken> Loggin(string password, string email)
        {
            var request = new RestRequest("sessions/create", Method.POST);
            request.AddParameter("password", password);
            request.AddParameter("email", email);
            return await _session.CreateSession(request);
        }


        public async Task<bool> RegisterUserB(string username, string password, string email)
        {
           var usertoken= await RegisterUser(username, password, email);
            if (usertoken != null || !String.IsNullOrEmpty(usertoken.IdToken))
            {
               await UpdateData();
                return true; }
            else return false;
        }

        public async Task<UserToken> RegisterUser(string username, string password, string email)
        {
            var request = new RestRequest("users", Method.POST);
            request.AddParameter("username", username);
            request.AddParameter("password", password);
            request.AddParameter("email", email);
            return await _session.CreateSession(request);
        }

        public async Task<IEnumerable<User>> GetUsersList(string filterTerm)
        {
            var request = new RestRequest("api/protected/users/list", Method.POST);
            request.AddParameter("filter", filterTerm);
            return await _session.Execute<List<User>>(request, true);
        }

        public async Task<bool> LogginB(string password, string email)
        {
            var usertoken = await Loggin(password, email);
            if (usertoken != null || !String.IsNullOrEmpty(usertoken.IdToken))
            {
                UpdateData();
                return true; }
            else return false;
        }

      
    }
}
