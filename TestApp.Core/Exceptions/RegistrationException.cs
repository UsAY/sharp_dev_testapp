﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestApp.Core.Exceptions
{
    public  class RegistrationException:Exception
    {
        private readonly string _message;

        public RegistrationException(string msg)
        {
            _message = msg;
        }
        public override string Message => _message;
    }
}
