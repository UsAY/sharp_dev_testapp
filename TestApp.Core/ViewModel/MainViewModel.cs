﻿using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestApp.Core.Model;
using TestApp.Core.Services.Interface;

namespace TestApp.Core.ViewModel
{
    public class MainViewModel : MvxViewModel
    {

        private readonly IPwUserService _pwUserService;
        private readonly IPWService _pWService;
        private readonly IMvxNavigationService _mvxNavigationService;

        public MainViewModel(IPwUserService pwUserService, IPWService pWService, IMvxNavigationService mvxNavigationService)
        {
            _pwUserService = pwUserService;
            _pWService = pWService;
            _mvxNavigationService = mvxNavigationService;

            CreateTransCommand = new MvxAsyncCommand(CreateTrans);
        }

        public override void Prepare()
        {
            _pwUserService.ChangeInfoEvent += (s, e) => RaiseAllPropertiesChanged();
        }

        private async void RepeatTransaction(TransTokenItem trans)
        {
            await CreateTransFromList(trans);
        }

        private async Task CreateTrans()
        {
            await _mvxNavigationService.Navigate<TransactionViewModel>();
        }
        private async Task CreateTransFromList(TransTokenItem item)
        {
           
            await _mvxNavigationService.Navigate<TransactionViewModel,TransTokenItem>(item);
        }

        private async Task UpdateUserData()
        {
            await _pwUserService.UpdateData();
            RaiseAllPropertiesChanged();
        }

        public string Id => _pwUserService.UserInfo.Id;
        public string UserName => _pwUserService.UserInfo.Name;
        public string Balance => _pwUserService.UserInfo.Balance;
        public string Email => _pwUserService.UserInfo.Email;
        public MvxObservableCollection<TransTokenItem> TransTokenItemsList => new MvxObservableCollection<TransTokenItem>(_pwUserService.TransList.TransTokens);

        public IMvxCommand CreateTransCommand { get; set; }

        private MvxCommand<TransTokenItem> _TransTokenItemClick;
        public MvxCommand<TransTokenItem> TransTokenItemClick => _TransTokenItemClick = _TransTokenItemClick ?? new MvxCommand<TransTokenItem>(RepeatTransaction);

      
    }
}
