﻿using Acr.UserDialogs;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestApp.Core.Model;
using TestApp.Core.Services.Interface;

namespace TestApp.Core.ViewModel
{
    public class TransactionViewModel:MvxViewModel<TransTokenItem>
    {

        private readonly IPWService _IPWService;
        private readonly IPwUserService _pwUserService;
        private readonly IMvxNavigationService _mvxNavigationService;
        TransTokenItem transactionData;

        public TransactionViewModel(IPWService pWService, IPwUserService pwUserService,IMvxNavigationService mvxNavigationService)
        {
            _IPWService = pWService;
            _pwUserService = pwUserService;
            _mvxNavigationService = mvxNavigationService;
            CreateTransCommand = new MvxAsyncCommand(CreateTransaction);
        }

        public override void Prepare(TransTokenItem parameter)
        {
            if (parameter == null) return;
            transactionData = parameter;
            RecipientName = transactionData.UserName;
            RecipientAmount = transactionData.Amount.ToString();
            
        }

        private async Task CreateTransaction()
        {
            if (String.IsNullOrEmpty(RecipientName) || _amount < 0)
            {
                await Mvx.Resolve<IUserDialogs>().AlertAsync("error input data", "ERROR", "Ok");
                return;
            }
            try
            {
                var transResult = await _IPWService.CreateTransaction(RecipientName,_amount);
                await _pwUserService.UpdateData();
                await Mvx.Resolve<IUserDialogs>().AlertAsync("transaction successful", "transaction", "Ok");
              await  _mvxNavigationService.Close(this);
            }
            catch (Exception ex)
            {
                await Mvx.Resolve<IUserDialogs>().AlertAsync(ex.Message, "ERROR", "OK");
            }
        }

        private async Task GetUserFromTerm()
        {
            try
            {
                Users = new MvxObservableCollection<User>(await _pwUserService.GetUsersList(RecipientName));
            }
            catch (Exception ex)
            {
                await Mvx.Resolve<IUserDialogs>().AlertAsync(ex.Message, "ERROR", "OK");
            }
        }

      

        private string _recipientName;
        public string RecipientName
        {
            get { return _recipientName; }
            set {
                _recipientName = value;
                if (_recipientName.Length > 1)  GetUserFromTerm();
                RaisePropertyChanged(RecipientName);
            }
        }

        private string _recipientAmount;
        public string RecipientAmount
        {
            get { return _recipientAmount; }
            set { _recipientAmount = value;
                double.TryParse(_recipientAmount, out _amount);
                RaisePropertyChanged(RecipientAmount);
            }
        }

        private double _amount;

        public IMvxCommand CreateTransCommand { get; set; }

        private MvxObservableCollection<User> _users;
        public MvxObservableCollection<User> Users
        {
            get => _users;
            set {
                _users = value;
                RaisePropertyChanged(() => Users);
            }
        }

        private object _SelectedUserObj;
        public object SelectedUserObj                  
        {
            get { return _SelectedUserObj; }
            private set
            {
                _SelectedUserObj = value;
                var user = (User)_SelectedUserObj;
                RecipientName = user.Name;
                RaisePropertyChanged(()=>SelectedUserObj);
            }
        }


    }
}
