﻿using Acr.UserDialogs;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestApp.Core.Services.Interface;

namespace TestApp.Core.ViewModel
{
   public class RegisterViewModel:MvxViewModel
    {
        private readonly IMvxNavigationService _mvxNavigationService;
        private readonly IPwUserService _pwUserService;

        public RegisterViewModel(IPwUserService pwUserService, IMvxNavigationService mvxNavigationService)
        {
            
            _pwUserService = pwUserService;
            _mvxNavigationService = mvxNavigationService;
            RegistrationCommand = new MvxAsyncCommand(RegisterUser);
        }

        private async Task RegisterUser()
        {
            if (String.IsNullOrEmpty(Email) || String.IsNullOrEmpty(Password) || (String.IsNullOrEmpty(UserName)&& !IsLogin))
            {
                await Mvx.Resolve<IUserDialogs>().AlertAsync("");
            }
            bool IsLoggining=false;
            try
            {
                if (!IsLogin)
                    IsLoggining = await _pwUserService.RegisterUserB(UserName, Password, Email);
                else
                    IsLoggining = await _pwUserService.LogginB(Password, Email);

                if (IsLoggining)
                {
                    await _mvxNavigationService.Navigate<MainViewModel>();
                }
            }
            catch (Exception ex)
            {
                await Mvx.Resolve<IUserDialogs>().AlertAsync(ex.Message, "ERROR", "OK");
            }

        }


        #region prop
        private string _userName;
        public string UserName
        {
            get{ return _userName; }
            set { _userName = value;
                RaisePropertyChanged(() => UserName);
            }
        }

        private string _email;
        public string Email
        {
            get { return _email; }
            set
            {
                _email = value;
                RaisePropertyChanged(() => Email);
            }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                RaisePropertyChanged(() => Password);
            }
        }

        private bool _isLogin;
        public bool IsLogin
        {
            get { return _isLogin; }
            set
            {
                _isLogin = value;
                RaisePropertyChanged(() => IsLogin);
            }
        }

        public IMvxCommand RegistrationCommand { get; set; }
        #endregion



    }
}
