﻿
using Acr.UserDialogs;
using MvvmCross;
using MvvmCross.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using TestApp.Core.Services;
using TestApp.Core.Services.Interface;
using TestApp.Core.ViewModel;

namespace TestApp.Core
{
   public class App: MvxApplication
    {
        public override void Initialize()
        {
            base.Initialize();


            Mvx.LazyConstructAndRegisterSingleton<ISession, Session>();
            Mvx.LazyConstructAndRegisterSingleton<IPwUserService, PwUserService>();
            Mvx.LazyConstructAndRegisterSingleton<IPWService, PWService>();
            
            Mvx.RegisterSingleton<IUserDialogs>(() => UserDialogs.Instance);




            RegisterAppStart<RegisterViewModel>();
        }
    }
}
